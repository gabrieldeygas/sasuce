//package com.lidem.cfa.taglib;
//
//import javax.servlet.jsp.JspException;
//import javax.servlet.jsp.JspWriter;
//import javax.servlet.jsp.tagext.TagSupport;
//import java.io.IOException;
//
//public class HelloWorldTag extends TagSupport {
//    @Override
//    public int doStartTag() throws JspException {
//        JspWriter out = pageContext.getOut();
//        try {
//            out.print("Hello World");
//        } catch (IOException e) {
//            throw new JspException("Error: " + e.getMessage());
//        }
//        return SKIP_BODY;
//    }
//}