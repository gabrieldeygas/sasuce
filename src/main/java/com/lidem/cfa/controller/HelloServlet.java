package com.lidem.cfa.controller;

import com.lidem.cfa.dao.PersonneService;
import com.lidem.cfa.dto.Personne;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet(name="HelloServlet", urlPatterns = {"/hello"})
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        PersonneService ps = new PersonneService();
        Personne jeanjean = new Personne("Jean", "Desesmorts");
        ps.addPersonne(jeanjean);

        String titre = "Bienvenue dans les jsp";
        Date today = new Date();
        req.setAttribute("date", today);
        req.setAttribute("titre", titre);
        req.setAttribute("personne", jeanjean);

        RequestDispatcher disp = req.getRequestDispatcher("bienvenue.jsp");
        disp.forward(req, resp);
    }
}
