package com.lidem.cfa.controller;


import com.lidem.cfa.dao.PersonneService;
import com.lidem.cfa.dto.Personne;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@WebServlet(name="PersonneServlet", urlPatterns = {"/personne"})
public class PersonneServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        PersonneService service = new PersonneService();
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne("Jean", "Paul"));
        personnes.add(new Personne("Jean", "Marc"));
        personnes.add(new Personne("Jean", "Pierre"));
        personnes.add(new Personne("Jean", "Roche"));
        personnes.add(new Personne("Jean", "Caillou"));

        for (Personne personne : personnes) {
            service.addPersonne(personne);
        }


        req.setAttribute("personnes", personnes);
        RequestDispatcher dispatcher = req.getRequestDispatcher("liste.jsp");
        dispatcher.forward(req, resp);
    }
}
