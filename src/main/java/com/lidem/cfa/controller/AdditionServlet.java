package com.lidem.cfa.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="AdditionServlet", urlPatterns = {"/addition"})
public class AdditionServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        printWriter(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        printWriter(req, resp);
    }


    private void printWriter(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = resp.getWriter();
        String num1 = req.getParameter("num1");
        String num2 = req.getParameter("num2");
        int result = Integer.parseInt(num1) + Integer.parseInt(num2);
        String textQ = "L'addition de " + num1 + " + " + num2 + " est de " + result;
        out.println("<html><head><title>J'en peux plus d'écrire</title></head>" +
                "<body><h2>" + textQ + "</h2></body></html>");
    }
}
