package com.lidem.cfa.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="private", urlPatterns = {"/private"})

public class PrivateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        PrintWriter out = resp.getWriter();
        session.setAttribute("connected", false);

        if (session.getAttribute("connected") != null && session.getAttribute("connected").equals(true)) {
            String html = "<html><head><title>Bravo t'es co</title></head>" +
                    "<body><h2>Private</h2></body></html>";
                    out.println(html);

        } else {
            String html = "<html><head><title>Dégage connaud</title></head>" +
                    "<body><h2>Private</h2></body></html>";
            out.println(html);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        PrintWriter out = resp.getWriter();
        if (session.getAttribute("connected") != null && session.getAttribute("connected").equals(true)) {
//            RequestDispatcher dispatcher = req.getRequestDispatcher("./private");
//            dispatcher.forward(req, resp);
            String identifiant = req.getParameter("identifiant");
            String password = req.getParameter("motdepasse");
            if(identifiant.equals("admin") && password.equals("admin")) {
                session.setAttribute("connected", true);
//                RequestDispatcher dispatcher = req.getRequestDispatcher("./private");
//                dispatcher.forward(req, resp);
                String html = "<html><head><title>C'est bon</title></head>" +
                        "<body><h2>C'est bon</h2></body></html>";
                out.println(html);

            } else {
                String html = "<html><head><title>C'est pas bon</title></head>" +
                        "<body><h2>C'est pas bon</h2></body></html>";
                out.println(html);
            }
        } else {

        }
    }

}
