package com.lidem.cfa.dao;

import com.lidem.cfa.dto.Personne;

import java.util.List;

public interface ServiceInterface {
    Personne findById(Integer id);
    List<Personne> findAll();
    Personne modifPersonne(Integer id, Personne p);

    void addPersonne(Personne p);

    Personne findByNameAndFirstname(String name, String Firstname);

    void delPersonne(Integer id);

}
