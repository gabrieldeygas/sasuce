package com.lidem.cfa.dao;

import com.lidem.cfa.dto.Personne;

import javax.persistence.EntityManager;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class PersonneService implements ServiceInterface {

    EntityManagerFactory factory;
    EntityManager manager;



    public PersonneService() {
       this.factory = Persistence.createEntityManagerFactory("UserDb");
       this.manager = factory.createEntityManager();
    }
    @Override
    public Personne findById(Integer id) {
        Personne p = new Personne();
        try {
            p = this.manager.find(Personne.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return p;
    }

    @Override
    public List<Personne> findAll() {
        List<Personne> personnes = new ArrayList<Personne>();
        try {
            Query query = this.manager.createQuery("select p from personne p", Personne.class);
            personnes = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personnes;
    }

    @Override
    public Personne modifPersonne(Integer id, Personne p) {
        Personne old = this.manager.find(Personne.class, id);
        this.manager.getTransaction().begin();
        old.setNom(p.getNom());
        old.setPrenom(p.getPrenom());
        this.manager.getTransaction().commit();
        this.manager.close();
        this.factory.close();
        return null;
    }

    @Override
    public void addPersonne(Personne p) {
        Personne doublon = this.findByNameAndFirstname(p.getNom(), p.getPrenom());
        if (doublon != null) {
            return;
        } else {

            try {
                this.manager.getTransaction().begin();
                this.manager.persist(p);
                this.manager.getTransaction().commit();
                this.manager.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Personne findByNameAndFirstname(String name, String firstname) {
        Personne p = new Personne();
        try {
            Query query = this.manager.createQuery("select p from Personne p where nom = :nom and prenom = :prenom", Personne.class);
            query.setParameter("nom", name);
            query.setParameter("prenom", firstname);
            p = (Personne) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    @Override
    public void delPersonne(Integer id) {
        Personne p = this.findById(id);
        try {
            this.manager.getTransaction().begin();
            this.manager.remove(p);
            this.manager.getTransaction().commit();
            this.manager.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
