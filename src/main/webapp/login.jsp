<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="./private" method="post">
        <div>
            <label for="identifiant">Identifiant</label>
            <input id="identifiant" type="text">
        </div>
        <div>
            <label for="motdepasse">Mot de passe</label>
            <input id="motdepasse" type="password" name="motdepasse">
        </div>
        <button type="submit">Se connecter</button>
    </form>
</body>
</html>