<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="./addition" method="post" class="">
    <p>
        <label for="ch1">Premier chiffre</label>
        <input id="ch1" type="number" name="num1" />
    </p>
    <p>
        <label for="ch2">Second chiffre</label>
        <input id="ch2" type="number" name="num2" />
    </p>
    <p>
        <input type="submit" value="Résultat" />
    </p>
</form>
</body>
</html>
