<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <tr>
        <th>Nom</th>
        <th>Prenom</th>
    </tr>
    <c:forEach items="${personnes}" var="p">
    <tr>
        <td>${p.nom}</td>
        <td>${p.prenom}</td>
    </tr>
    </c:forEach>

</table>
<h2></h2>
</body>
</html>