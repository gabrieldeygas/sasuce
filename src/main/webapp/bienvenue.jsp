<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>S
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<%@ taglib prefix="hello" uri="/WEB-INF/lib/mesTaglib.tld" %>--%>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>${titre}</h2>
<h3>
    <fmt:bundle basename="message">
        <fmt:message key="msg"></fmt:message>
    </fmt:bundle>
</h3>
<p>
    <fmt:formatDate value="${date}" type="date" pattern="dd-MM-YYYY"></fmt:formatDate>
</p>
<p>
<%--    <hello:helloWorld/>--%>

</p>
<p>
    <h5>${personne.fullName()}</h5>
    ${personne.nom}
</p>
</body>
</html>